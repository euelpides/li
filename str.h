#pragma once
#include<stdio.h>

typedef struct str
{
	size_t size;
	char*ptr;
} str;

// Allocate string
str str_create(char*p);

// Free internal memory
void str_free(str*s);

// Concatenate strings
void str_concat(str*s,char*p);

// Assign/reassign to string
void str_assign(str*s,char*p);

// String contains whitespace
int str_ws(str*s);
