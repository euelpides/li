CC=      tcc
CFLAGS=  -Wfatal-errors
LDFLAGS= -s -lm
RM=      rm -f

all: li
test: test.c vec.c str.c
	$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS)
li: main.c vec.c str.c
	$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS)
clean:
	$(RM) *.o li test
.PHONY: all clean
